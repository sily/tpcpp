#pragma once

#include <stdexcept>
template<typename T>

class PileGen
{
private:
    T *_array;
    int _capacity;
    int _topIndex;

public:
    PileGen();
    PileGen(int);
    ~PileGen();

    bool empty(void) const;
    void push(const T);
    T pop(void);
    T top(void) const;
    int size(void) const;
};

template<typename T>
PileGen<T>::PileGen() : PileGen(256)
{
}

template<typename T>
PileGen<T>::PileGen(int capacity) : _array(nullptr), _capacity(capacity), _topIndex(0)
{
	if (capacity <= 0)
		throw std::invalid_argument("capacity can't be negative or null");
	_array = new int[capacity];
}

template<typename T>
PileGen<T>::~PileGen()
{
	delete[] _array;
}

template<typename T>
bool PileGen<T>::empty(void) const
{
	return _topIndex == 0;
}

template<typename T>
void PileGen<T>::push(const T value)
{
	if (_topIndex >= _capacity)
		throw std::overflow_error("stack is full");
	_array[_topIndex] = value;
	_topIndex++;
}

template<typename T>
T PileGen<T>::pop(void)
{
	if (_topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	_topIndex--;
	return _array[_topIndex + 1];
}

template<typename T>
T PileGen<T>::top(void) const
{
	if (_topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	return _array[_topIndex - 1];
}

template<typename T>
int PileGen<T>::size(void) const
{
	return _topIndex;
}