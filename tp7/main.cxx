#include <iostream>
#include <fstream>

template <typename T>

const T& maximum(const T& a, const T& b)
{
    return ((a > b) ? a : b);
}

int main(int, char **)
{
    int x = 2;
    int y = 3;

    float x1 = 2.2;
    float y1 = 3.3;

    std::cout << maximum(x,y) << std::endl;
    // std::cout << maximum(x1,y) << std::endl;
    std::cout << maximum(x1,y1) << std::endl;
    return 0;
}