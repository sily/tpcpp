#pragma once

#include <iostream>

using std::cout;
using std::endl;
using std::string;

class Machine
{
private:
    string _type;
    int _autonomie;
    int _Ifixit;

public:
    Machine(string, int, int);
    string getType() const;
    int getAutonomie() const;
    int getIfixit() const;
};