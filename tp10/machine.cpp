#include "machine.hpp"


Machine::Machine(string type, int autonomie, int ifixit) : _type(type), _autonomie(autonomie), _Ifixit(ifixit)
{

}

string Machine::getType() const
{
    return _type;
}

int Machine::getAutonomie() const
{
    return _autonomie;
}

int Machine::getIfixit() const{
    return _Ifixit;
}