#include "catch.hpp"
#include <cstring>
#include <sstream> // a mettre en commentaire
#include "humain.hpp"
#include "machine.hpp"
#include "humanoid.hpp"

TEST_CASE("Humain1")
{
  const char *nom = "Alex";
  const Humain alex(nom, GENRE::HOMME, 35);

  CHECK(nom == alex.getNom());
  CHECK(GENRE::HOMME == alex.getGenre());
  // ou enum class Genre.HOMME
  CHECK(35 == alex.getAge());
}

TEST_CASE("Humain2")
{
  Humain thomas("thomas", GENRE::HOMME, 26);

  thomas.setNom("conchita");
  thomas.setAge(27);
  thomas.setGenre(GENRE::FEMME);

  CHECK("conchita" == thomas.getNom());
  CHECK(GENRE::FEMME == thomas.getGenre());
  CHECK(27 == thomas.getAge());
}

TEST_CASE("Machine")
{
  Machine stylet("stylet Apple", 2 * 24 * 3600, 1);

  CHECK("stylet Apple" == stylet.getType());
  CHECK(2 * 24 * 3600 == stylet.getAutonomie());
  CHECK(1 == stylet.getIfixit());
}

TEST_CASE("Robocop")
{
  Humanoid robocop("Murphy", "Robocop 1.0", GENRE::HOMME, 35);

  CHECK("Murphy" == robocop.getNom());
  CHECK("Robocop 1.0" == robocop.getType());
  CHECK(GENRE::HOMME == robocop.getGenre());
  CHECK(35 == robocop.getAge());
  CHECK(3 == robocop.getIfixit());
}