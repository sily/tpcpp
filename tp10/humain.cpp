#include "humain.hpp"

Humain::Humain(string nom, GENRE genre, int age) : _nom(nom), _genre(genre), _age(age)
{
}

int Humain::getAge() const
{
  return _age;
}

GENRE Humain::getGenre() const
{
  return _genre;
}

string Humain::getNom() const
{
  return _nom;
}

void Humain::setAge(int age)
{
  _age=age;
}

void Humain::setGenre(GENRE genre)
{
  _genre=genre;
}

void Humain::setNom(string nom)
{
  _nom=nom;
}