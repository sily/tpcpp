#pragma once
#include <iostream>

using std::cout;
using std::endl;
using std::string;

enum class GENRE
{
    HOMME,
    FEMME
};

class Humain
{
private:
    string _nom;
    GENRE _genre;
    int _age;

public:
    Humain(string, GENRE, int);
    string getNom() const;
    GENRE getGenre() const;
    int getAge() const;
    void setNom(string);
    void setGenre(GENRE);
    void setAge(int);
};