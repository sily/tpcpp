#pragma once

#include <iostream>

#include "humain.hpp"
#include "machine.hpp"

class Humanoid : public Humain, public Machine {
    public:
    Humanoid(string, string, GENRE, int, int = 3);
};