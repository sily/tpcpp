#include "Vecteur.hpp"
#include "null_pointer_exception.hpp"
#include <cstring>
#include <exception>
#include <iostream>
#include <new>
#include <ostream>
#include <stdexcept>

Vecteur::Vecteur(const int capacity)
try : _array(new double[capacity]),
	_capacity(capacity),
	_size(0)
{
}
catch (std::exception &e)
{
	std::cerr << e.what() << std::endl;
}

Vecteur::Vecteur() : Vecteur(100000)
{
}

Vecteur::Vecteur(const Vecteur &v) : Vecteur(v._capacity)
{
	if (v._array == nullptr)
		throw null_pointer_exception();
	std::copy(v._array, v._array + v._capacity, _array);
	_size = v._size;
}

Vecteur::~Vecteur()
{
	delete[] _array;
}

void Vecteur::push_back(const double value)
{
	if (_capacity == _size)
	{
		double *newArray = new double[_capacity * 2];
		std::copy(_array, _array + _capacity, newArray);
		delete[] _array;
		_array = newArray;
		_capacity *= 2;
	}
	_array[_size] = value;
	_size++;
}

Vecteur &Vecteur::operator=(const Vecteur &v)
{
	delete[] _array;
	_array = new double[v._capacity];
	std::copy(v._array, v._array + v._capacity, _array);
	_capacity = v._capacity;
	_size = v._size;
	return *this;
}

double &Vecteur::operator[](const int i)
{
	if (_array == nullptr)
		throw null_pointer_exception();
	if (i >= _capacity)
		throw std::out_of_range("out of bound");
	return _array[i];
}

double Vecteur::operator[](const int i) const
{
	if (_array == nullptr)
		throw null_pointer_exception();
	if (i >= _capacity)
		throw std::out_of_range("out of bound");
	return _array[i];
}

int Vecteur::capacity(void) const
{
	return _capacity;
}

int Vecteur::size(void) const
{
	return _size;
}

std::ostream &operator<<(std::ostream &os, Vecteur &v)
{
	os << "[";
	for (int i = 0; i < v.size(); i++)
	{
		os << v[i];
		if (i != v.size() - 1)
			os << ", ";
	}
	os << "]";
	return os;
}