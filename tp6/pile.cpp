#include "pile.hpp"
#include <stdexcept>

Pile::Pile() : Pile(10)
{
}

Pile::Pile(int capacity) : _array(nullptr), _capacity(capacity), _topIndex(0)
{
	if (capacity <= 0)
		throw std::invalid_argument("capacity can't be negative or null");
	_array = new int[capacity];
}

Pile::~Pile()
{
	delete[] _array;
}

bool Pile::empty(void) const
{
	return _topIndex == 0;
}

void Pile::push(const int value)
{
	if (_topIndex >= _capacity)
		throw std::overflow_error("stack is full");
	_array[_topIndex] = value;
	_topIndex++;
}

int Pile::pop(void)
{
	if (_topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	_topIndex--;
	return _array[_topIndex + 1];
}

int Pile::top(void) const
{
	if (_topIndex <= 0)
		throw std::invalid_argument("stack is empty");
	return _array[_topIndex - 1];
}

int Pile::size(void) const
{
	return _topIndex;
}
