#include "Chaine.hpp"
#include <cstring>

Chaine::Chaine() : _capacite(-1), _tab(nullptr)
{
}

Chaine::Chaine(const char *tab) : _capacite(std::strlen(tab))
{
    _tab = new char[_capacite];
    std::strcpy(_tab, tab);
}

Chaine::Chaine(int capacite) : _capacite(capacite)
{
    _tab = new char[_capacite];
    if (_capacite)
    {
        _tab[0] = 0;
    }
}

Chaine::Chaine(const Chaine &uC)
{
    std::cout << "constructeur de copie appelée" << std::endl;
    _capacite = uC.getCapacite();
    if (_capacite)
    {
        _tab = new char[_capacite];
        strcpy(_tab, uC.c_str());
    }
    else
    {
        _tab = nullptr;
    }
}

Chaine::~Chaine()
{
    delete[] _tab;
}

int Chaine::getCapacite() const
{
    return _capacite;
}

char *Chaine::c_str() const
{
    return _tab;
}

void Chaine::afficher(std::ostream &stream) const
{
    stream << _tab;
}

Chaine &Chaine::operator=(const Chaine &uC)
{
    if (&uC != this)
    {
        delete[] _tab;
        _capacite = uC._capacite;
        if (_capacite)
        {
            _tab = new char[_capacite];
            strcpy(_tab, uC._tab);
        }

        else
        {
            _tab = 0;
        }
    }

    return *this;
}

std::ostream &operator<<(std::ostream &os, const Chaine &chaine)
{
    if (chaine.c_str())
    {
        os << chaine.c_str();
    }

    return os;
}

char& Chaine::operator[](int index)
{
    if ((index<0) || (index>=_capacite))
    {
        std::cout << "erreur index" << std::endl;
        std::exit(1);
    }

    return _tab[index];
}

char Chaine::operator[](int index) const
{
    if ((index<0) || (index>=_capacite))
    {
        std::cout << "erreur index" << std::endl;
        std::exit(1);
    }

    return _tab[index];
}

Chaine operator+(Chaine lhs, const Chaine &rhs)
{
	int len = 1;
	len += strlen(lhs._tab);
	len += strlen(rhs._tab);
	Chaine concat = Chaine(len);
	strcpy(concat._tab, lhs._tab);
	strcat(concat._tab, rhs._tab);
	return concat;
}