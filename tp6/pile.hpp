#ifndef PILE_H
#define PILE_H

class Pile
{
private:
    int *_array;
    int _capacity;
    int _topIndex;

public:
    Pile();
    Pile(int);
    ~Pile();

    bool empty(void) const;
    void push(const int value);
    int pop(void);
    int top(void) const;
    int size(void) const;
};

#endif