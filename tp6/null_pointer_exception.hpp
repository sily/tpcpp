#ifndef NULL_POINTER_EXCEPTION
#define NULL_POINTER_EXCEPTION

#include <stdexcept>

class null_pointer_exception : public std::exception
{
public:
  virtual const char *what() const noexcept override;
};

#endif