#include "null_pointer_exception.hpp"

const char *null_pointer_exception::what() const noexcept
{
    return "Pointer was null";
}
