#ifndef VECTEUR_H
#define VECTEUR_H

#include "ostream"

class Vecteur
{
private:
	double *_array;
	int _capacity;
	int _size;

public:
	Vecteur(const int capacity);
	Vecteur();
	Vecteur(const Vecteur &v);
	~Vecteur();
	void push_back(const double value);
	Vecteur &operator=(const Vecteur &v);
	double &operator[](const int i);
	double operator[](const int i) const;
	int capacity(void) const;
	int size(void) const;
};

std::ostream &operator<<(std::ostream &os, Vecteur &v);

#endif