#ifndef CPP5__CHAINE_HPP
#define CPP5__CHAINE_HPP

#include <iostream>
#include <fstream>

class Chaine
{
private:
    int _capacite;
    char *_tab;

public:
    Chaine();
    Chaine(const char *);
    Chaine(int);
    Chaine(const Chaine &);
    ~Chaine();
    int getCapacite() const;
    char *c_str() const;
    void afficher(std::ostream & = std::cout) const;
    Chaine &operator=(const Chaine &);
    char& operator[](int);
    friend std::ostream &operator<<(std::ostream &, const Chaine &);
};
std::ostream &operator<<(std::ostream &, const Chaine &);

#endif
