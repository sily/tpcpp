#pragma once
#include <string>

#include "Point.hpp"

enum class COULEURS
{
    NOIR,
    BLANC,
    BLEU,
    ROUGE,
    VERT,
    JAUNE
};

class Forme
{

private:
    static int _nbFormes;
    int _w;
    int _h;
    static int _id;
    Point _point;
    COULEURS _couleur;

public:
    Forme(int = 0, int = 0);
    Forme(int, int, int, int, COULEURS = COULEURS::BLEU);
    Forme(Point, COULEURS);
    virtual ~Forme();
    int getW();
    int getH();
    void setW(int);
    void setH(int);
    void setX(int);
    void setY(int);
    Point &getPoint();
    COULEURS getCouleur();
    void setCouleur(COULEURS);
    static int getId();
    static int prochainId();
    virtual std::string toString();
    virtual int essai()=0;
};