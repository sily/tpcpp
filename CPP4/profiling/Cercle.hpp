#pragma once

#include <string>
#include "Forme.hpp"

class Cercle : public Forme
{
    int _rayon;
    // int _ordre;

public:
    Cercle(/*Liste,*/ int = 0, int = 0, int = 0, int = 0);
    Cercle(/*Liste,*/ int, int, int);
    virtual ~Cercle();
    Cercle();

    int getRayon();
    void setRayon(int);

    virtual std::string toString() override;
};