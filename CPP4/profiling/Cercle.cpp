// Fichier Cercle.cpp
#include <sstream>
#include <string>
#include <iostream>

#include "Cercle.hpp" // Inclusion d'un fichier du répertoire courant

Cercle::Cercle(/*Liste L,*/ int x, int y, int w, int h) : Forme(x, y, w, h), _rayon(w / 2) /*_ordre(L.getCompteur())*/
{
  // ++L._compteur;
  // L._listeCercles[L._nb_c] = *this;
  // ++L._nb_c;
}

Cercle::Cercle(/*Liste L,*/ int x, int y, int rayon) : Cercle(x, y, 2 * rayon, 2 * rayon)
{
  // L._listeCercles[L._nb_c] = *this;
}

Cercle::~Cercle()
{
}

int Cercle::getRayon()
{
  return _rayon;
}

void Cercle::setRayon(int rayon)
{
  _rayon = rayon;
  setW(2*rayon);
  setH(2*rayon);
}

std::string Cercle::toString()
{
	std::stringstream ss;
	ss << "CERCLE " << getPoint().toString() << " " << getW() << " " << getH();
	return ss.str();
}