#pragma once
#include <string>

// #include "Rectangle.hpp"
// #include "Cercle.hpp"

#include "Forme.hpp"
#include <vector>
#include <string>

class Groupe : public Forme {
private:
    std::vector<Forme*> formes;

public:
    Groupe();
    virtual ~Groupe();
    void add(Forme *forme);
    virtual std::string toString(void) override;
    Forme* operator[](std::size_t i);
	const Forme* operator[](std::size_t i) const;
    std::size_t size(void) const;

};
