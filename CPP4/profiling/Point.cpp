#include "Point.hpp"

#include <sstream>

Point::Point(int x, int y) : _x(x), _y(y)
{

}

int Point::getX()
{
    return _x;
}


int Point::getY()
{
    return _y;
}

void Point::setX(int x)
{
    _x = x;
}

void Point::setY(int y)
{
    _y = y;
}


std::string Point::toString() const {
	std::stringstream ss;
	ss << "{" << _x << "," << _y << "}";
	return ss.str();
}

const Point Point::ORIGINE = Point{};