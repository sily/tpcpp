// Fichier Rectangle.cpp

#include <iostream>  // Inclusion d'un fichier standard
#include "Rectangle.hpp" // Inclusion d'un fichier du répertoire courant

Rectangle::Rectangle(/*Liste L,*/ int x, int y, int w, int h) : _x(x), _y(y), _w(w), _h(h) /*, _ordre(L.getCompteur())*/
{
  // ++L._compteur;
  // L._listeRectangles[L._nb_r] = *this;
  // ++L._nb_r;
}

int Rectangle::getX()
{
  return _x;
}

void Rectangle::setX(int x)
{
  _x = x;
}

int Rectangle::getY()
{

  return _y;
}

void Rectangle::setY(int y)
{
  _y = y;
}

int Rectangle::getW()
{
  return _w;
}

void Rectangle::setW(int w)
{
  _w = w;
}

int Rectangle::getH()
{

  return _h;
}

void Rectangle::setH(int h)
{
  _h = h;
}


std::string Rectangle::toString()
{
    return "RECTANGLE "+std::to_string(_x)+" "+std::to_string(_y)+" "+std::to_string(_w)+" "+std::to_string(_h);
}