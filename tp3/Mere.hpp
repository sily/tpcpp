#pragma once

#include <iostream>

class Mere
{
private:
    int _jpp;
    static int _counter;

public:
    ~Mere();
    Mere();
    Mere(int);
    int getJpp();
    int getCounter();
};