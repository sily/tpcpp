#pragma once
#include "Mere.hpp"

class Fille : public Mere
{
private:
    int _ff;

public:
    ~Fille();
    Fille(int);
    int getff();
};