#pragma once
#include <iostream>

class B;

class A
{
private:
    int _i;

public:
    A(int);
    int getI();
    void send(B *);
    void exec(int);
};