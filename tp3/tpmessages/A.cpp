#include "A.hpp"
#include "B.hpp"

A::A(int i) : _i(i)
{

}

void A::exec(int n)
{
    _i += n;
}

void A::send(B* unB)
{
    unB->exec(2);
}

int A::getI()
{
    return _i;
}