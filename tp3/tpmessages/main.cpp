#include "A.hpp"
#include "B.hpp"

int main(int, char **)
{
    B unB(2);
    A unA(10);

    unB.send(&unA);
    unA.send(&unB);

    std::cout << " A : " << unA.getI() << " B : " << unB.getJ() << std::endl;

    return 0;
}