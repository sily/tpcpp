#include "B.hpp"
#include "A.hpp"

B::B(int j) : _j(j)
{

}

void B::exec(int n)
{
    _j += n;
}

void B::send(A* unA)
{
    unA->exec(3);
}

int B::getJ()
{
    return _j;
}