#pragma once
#include <iostream>

class A;

class B
{
private:
    int _j;

public:
    B(int);
    void send(A *);
    void exec(int);
    int getJ();
};