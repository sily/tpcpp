#include "Mere.hpp"

int Mere::_counter = 0;

Mere::Mere(int n=0) : _jpp(n)
{
    std::cout << "construction de maman : " << _jpp << std::endl;
    ++_counter;
}

int Mere::getJpp()
{
    return _jpp;
}

Mere::~Mere()
{
    std::cout << "destruction de maman : " << _jpp << std::endl;
}

int Mere::getCounter()
{
    return _counter;
}
