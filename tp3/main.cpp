//g++ main.cpp -o main -Wall -Wextra -g

#include <iostream>
#include "Fille.hpp"

class Bavarde
{
private:
    int _numero;

public:
    int getNum()
    {
        return _numero;
    }

    Bavarde(int numero = 0) : _numero(numero)
    {

        std::cout << "construction de " << numero << std::endl;
    }

    ~Bavarde()
    {
        std::cout << "Tais-toi " << _numero << std::endl;
    }

    void afficher()
    {
        std::cout << "affichage de : " << _numero << std::endl;
    }

} /*bizarre(1)*/;

class Couple
{
private:
    Bavarde _giselle;
    Bavarde _jeanne;

public:
    Couple() : _giselle(Bavarde(3)), _jeanne(Bavarde(6))
    {
        // _giselle = Bavarde();
        // _jeanne = Bavarde();
        std::cout << "affichage de : " << std::endl;
    }
};

class Famille
{
private:
    Bavarde *_gang;

public:
    Famille(int n) : /*_gang((Bavarde *)malloc(n * sizeof(Bavarde)))*/ _gang(new Bavarde[n])
    {
    }
    ~Famille()
    {
        // free(_gang);
        delete[] _gang;
    }
};

// Bavarde globale(2);

void fonction(Bavarde b)
{
    std::cout << "code de la fonction" << std::endl;
}

int main(int, char **)
{
    // Bavarde b;
    // Bavarde *p = new Bavarde(3);
    // fonction(b);
    // delete p;

    // std::cout << Bavarde(0).getNum() << std::endl;

    // const int TAILLE = 20;
    // Bavarde tab1[TAILLE];
    // Bavarde *tab2 = new Bavarde[TAILLE];
    // for (int i = 0; i < TAILLE; ++i)
    // {
    //     tab1[i].afficher();
    //     tab2[i].afficher();
    // }
    // delete[] tab2;

    // Couple chiant;

    // Famille rapideetfurieux(10);

    // Bavarde *b1 = (Bavarde *)malloc(1 * sizeof(Bavarde));
    // b1->afficher();
    // free(b1);

    // Bavarde * b2 = new Bavarde(20);
    // b2->afficher();
    // delete b2;

    Fille fille(2);
    
    std::cout << "compteur : " << fille.getCounter() << std::endl;

    return 0;
}