#include "producteur.hpp"
#include <fstream>

Producteur::Producteur() : _travail(0)
{
}

int Producteur::getTravail()
{
    return _travail;
}

bool Producteur::produire(int quantite, std::string nom)
{
    std::ofstream fichier(nom.c_str());
    bool res = !fichier.fail();
    if (res)
    {
        fichier << quantite << std::endl;
        for (int i = 0; i < quantite; ++i)
        {
            fichier << i + 1 << std::endl;
        }
    }

    ++_travail;
    fichier.close();
    return res;
}
