#include "statisticien.hpp"
#include <fstream>

Statisticien::Statisticien() : _calcul(false)
{

}

bool Statisticien::aCalcule()
{
    return _calcul;
}

void Statisticien::acquerir(std::string nom)
{
    std::ifstream fichier; // autre manière d'ouvrir un fichier
    int i = 0, max;

    fichier.open(nom.c_str());
    // fail() pas testé :-(

    fichier >> max;

    while (!fichier.eof() && i < max)
    {
        double lecture;
        fichier >> lecture;
        ++i;
        std::cout << lecture << " ";
    }
    fichier.close();
}
