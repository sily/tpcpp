#include "Fille.hpp"

Fille::Fille(int n=0) : Mere(2342),_ff(n)
{
    std::cout << "construction de fille : " << _ff << std::endl;
}

int Fille::getff()
{
    return _ff;
}

Fille::~Fille()
{
    std::cout << "destruction de fille : " << _ff << std::endl;
}