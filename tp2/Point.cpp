// Fichier Point.cpp

#include <iostream>  // Inclusion d'un fichier standard
#include "Point.hpp" // Inclusion d'un fichier du répertoire courant

int Point::cpt = 0;

int Point::getX()
{
  return _x;
}

void Point::setX(int val)
{
  _x = val;
}

int Point::getY()
{

  return _y;
}

void Point::setY(int val)
{
  _y = val;
}

void Point::deplacerDe(int x, int y)
{
  _x += x;
  _y += y;
}

void Point::deplacerVers(int x, int y)
{
  _x = x;
  _y = y;
}

Point::Point()
{
  std::cout << "Pas d'arguments" << std::endl;
  cpt++;
}

Point::Point(int x, int y) : _x(x), _y(y)
{
  std::cout << "avec argument" << std::endl;
  cpt++;
}

int Point::getCpt()
{
  return cpt;
}