// Fichier main.cpp
#include "Point.hpp"
#include <iostream>

int main(int, char **)
{
    Point p1;
    Point p2{6, 9};
    Point *p3 = new Point(4, 2);
    Point *p4 = new Point();
    //   std::cout << p2.getX() << std::endl;
    std::cout << p1.getCpt() << std::endl;
    std::cout << Point::getCpt() << std::endl;
    delete p3;
    delete p4;

    return 0;
}