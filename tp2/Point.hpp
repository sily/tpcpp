#ifndef ZZPOINT_HPP
#define ZZPOINT_HPP
// Fichier Point.hpp
// Il manque les gardiens mais je vous laisse les ajouter,
// c'est comme en C

class Point
{
    // par défaut, tout est privé dans une "class"

    int _x;
    int _y;
    static int cpt;

public:
    Point();
    Point(int, int);
    int getX();
    void setX(int);
    int getY();
    void setY(int);
    void deplacerDe(int, int);
    void deplacerVers(int, int);
    static int getCpt();
};

#endif