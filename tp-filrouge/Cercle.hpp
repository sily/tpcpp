#pragma once

#include <string>
#include "Forme.hpp"

class Cercle : public Forme
{
    int _rayon;
    // int _ordre;

public:
    Cercle();
    Cercle(/*Liste,*/ int, int, int, int);
    Cercle(/*Liste,*/ int, int, int);
    virtual ~Cercle();

    int getRayon();
    void setRayon(int);

    virtual std::string toString() override;
    virtual Forme *clone() override;
};