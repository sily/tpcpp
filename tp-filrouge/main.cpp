// Fichier main.cpp
#include "Rectangle.hpp"
#include "Cercle.hpp"
#include <iostream>

// void afficher1(Forme f) {
//    f.afficher();
// }

// void afficher2(Forme &f) {
//    f.afficher();
// }

// void afficher3(Forme * f) {
//    f->afficher();
// }

// int main(int, char**) {
//    Cercle cercle;
   
//    afficher1(cercle);
//    afficher2(cercle);
//    afficher3(&cercle);
   
//    return 0;
// }

int main(int, char **)
{
    Rectangle rec(30, 30, 15, 15);
    Cercle cer(10, 10, 30, 30);
    std::cout << cer.toString() << std::endl;
    std::cout << rec.toString() << std::endl;

    return 0;
}