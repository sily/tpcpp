#pragma once

#include <string>

class Point
{
    private:

    int _x;
    int _y;

    public:
    Point(int=0, int=0);
    int getX() const;
    int getY() const;
    void setX(int);
    void setY(int);
    std::string toString() const;
	static const Point ORIGINE;
    
};