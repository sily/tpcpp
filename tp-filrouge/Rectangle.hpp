#pragma once
#include <string>
#include "Forme.hpp"

class Liste;

class Rectangle : public Forme
{
private:
    int _x;
    int _y;
    int _w;
    int _h;
    // int _ordre;

public:
    Rectangle(/*Liste,*/ int=0, int=0, int=0, int=0);
    Rectangle();
    int getX();
    void setX(int);
    int getY();
    void setY(int);
    int getW();
    void setW(int);
    int getH();
    void setH(int);
    std::string toString();
    Forme * clone();
};
