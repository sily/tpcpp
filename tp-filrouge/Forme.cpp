#include "Forme.hpp"

int Forme::_id = -1;

Forme::Forme(int x, int y, int w, int h, COULEURS couleur) : _w(w), _h(h), _point(Point(x, y)), _couleur(couleur)
{
    ++_id;
}

Forme::Forme(int w, int h) : _w(w), _h(h), _point(Point::ORIGINE), _couleur(COULEURS::BLEU)
{
    ++_id;
}

Forme::Forme(Point point, COULEURS couleur) : _w(0), _h(0), _point(point), _couleur(couleur)
{
    _point.setX(point.getX());
    _point.setY(point.getY());
    ++_id;
}

Forme::~Forme() {
}


int Forme::getH() const
{
    return _h;
}

int Forme::getW() const
{
    return _w;
}

int Forme::getId()
{
    return _id;
}

int Forme::prochainId()
{
    return _id + 1;
}

// std::string Forme::toString()
// {
//     return "FORME " + std::to_string(_w) + " " + std::to_string(_h);
// }

Point &Forme::getPoint() 
{
    return _point;
}

COULEURS Forme::getCouleur()
{
    return _couleur;
}

void Forme::setX(int x)
{
    _point.setX(x);
}

void Forme::setY(int y)
{
    _point.setY(y);
}

int Forme::getX() const
{
    return _point.getX();
}

int Forme::getY() const
{
    return _point.getY();
}

void Forme::setCouleur(COULEURS couleur)
{
    _couleur = couleur;
}

void Forme::setH(int h)
{
    _h=h;
}

void Forme::setW(int w)
{
    _w=w;
}

void Forme::afficher()
{
    std::cout << toString() << std::endl;
}